﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using gcsd;

namespace UnitTest.gcsd.connection
{
    [TestClass]
    public class ConversationData
    {
        #region Variables

        private PureCloud _pureCloud;

        #endregion

        #region TestInitialize

        [TestInitialize]
        public void TestInitialize()
        {
            _pureCloud = new PureCloud
            {
                ClientId = StaticConfig.ClientId,
                ClientSecret = StaticConfig.ClientSecret,
                Environment = StaticConfig.Environment
            };

            _pureCloud.Login();
        }

        #endregion

    }
}
