﻿using PureCloudPlatform.Client.V2.Model;

namespace gcsd.Model
{
    public class SegmentAttr
    {
        public string ConversationId { get; set; }
        public string ParticipantId { get; set; }
        public Segment Segment { get; set; }
    }
}
