﻿using System;
using System.Collections.Generic;

namespace gcsd.plugins
{
    public interface IPlugin
    {
        /// <summary>
        /// Called when the plugin is first loaded
        /// </summary>
        void Initialize(string[] cmdArgs);

        /// <summary>
        /// Gets the command line parameters used by this plugin
        /// </summary>
        /// <returns>A list of command line parameters that this plugin is used for</returns>
        string[] GetCommandLineParameters();

        /// <summary>
        /// Gets the command line parameters help used by this plugin
        /// </summary>
        /// <returns>A list of command line parameters help that this plugin is used for</returns>
        string[] GetCommandLineParametersHelp();

        /// <summary>
        /// Reset the interval to a timespan maxSpan days in the past.
        /// </summary>
        void ResetInterval(DateTime dateTimeNow);

        /// <summary>
        /// Get the interval when the plugin was last executed
        /// </summary>
        /// <returns>A DateTime with the interval that was used during the last execution of ths plugin</returns>
        DateTime GetLatestInterval();

        /// <summary>
        /// Get the interval for aggregates when the plugin was last executed
        /// </summary>
        /// <returns>A DateTime with the aggregate interval that was used during the last execution of ths plugin</returns>
        DateTime GetLatestIntervalForAggregates();

        /// <summary>
        /// Set the interval when the plugin was last executed
        /// </summary>
        void SetLatestInterval(DateTime dateTime);

        /// <summary>
        /// Set the interval for aggregates when the plugin was last executed
        /// </summary>
        void SetLatestIntervalForAggregates(DateTime dateTime);

        /// <summary>
        /// Called when dictionaries should be initialized (i.e. Conversations, Queues, Users)
        /// </summary>
        void InitializeDictionaries(Dictionary<string, string> queues, Dictionary<string, string> languages, Dictionary<string, string> skills, Dictionary<string, string> users, Dictionary<string, string> wrapUpCodes, Dictionary<string, string> edgeServers, Dictionary<string, string> campaigns, Dictionary<string, string> contactLists, Dictionary<string, string> systemPresence);

        /// <summary>
        /// Called when data needs to be pushed to the 3rd party system (target)
        /// </summary>
        /// <param name="data">data (json-formatted) to push</param>
        /// <returns>True if successful, false otherwise</returns>
        bool PushData(string data);

        /// <summary>
        /// Called when the main applications exits
        /// </summary>
        void Dispose();

        /// <summary>
        /// This method returns a list of identifiers of reported previously ongoing conversations. They have to be retrived from the PC API and pushed into the target if already finished.
        /// </summary>
        /// <returns>List of conversation identifiers.</returns>
        List<string> OngoingConversationIdList();

        /// <summary>
        /// Returns the list of stats that are supported by the plugin.
        /// </summary>
        /// <returns>The list of stats</returns>
        List<string> SupportedStats();

        bool AttachParticipantAttrs { get; }
    }
}
