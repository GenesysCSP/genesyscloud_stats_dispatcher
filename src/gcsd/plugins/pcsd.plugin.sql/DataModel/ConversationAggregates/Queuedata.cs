﻿using System.Collections.Generic;

namespace gcsd.plugin.sql.DataModel.ConversationAggregates
{
    class Queuedata
    {
        public Group group { get; set; }
        public IList<Datum> data { get; set; }
    }
}
