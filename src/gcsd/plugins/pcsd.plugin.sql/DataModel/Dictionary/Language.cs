﻿using System.ComponentModel.DataAnnotations;

namespace gcsd.plugin.sql.DataModel.Dictionary
{
    public class Language: IDictionaryItem
    {
        [Key]
        public string id { get; set; }
        public string name { get; set; }
    }
}
