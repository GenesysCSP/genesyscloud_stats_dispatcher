﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using gcsd.plugin.sql.DataModel.UserDetails;

using log4net;

namespace gcsd.plugin.sql.DataModel.Dictionary
{
    public class DictionaryManager
    {
        private static readonly ILog Trace = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void SaveQueues(Dictionary<string, string> queues, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in queues)
                    {
                        if (dbCtx.Queues.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.Queues.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == q.Value) continue;
                            existingItem.name = q.Value;
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.Queues.Add(new Queue() { id = q.Key, name = q.Value });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Queues added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SaveLanguages(Dictionary<string, string> languages, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in languages)
                    {
                        if (dbCtx.Languages.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.Languages.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == q.Value) continue;
                            existingItem.name = q.Value;
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.Languages.Add(new Language() { id = q.Key, name = q.Value });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Languages added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SaveSkills(Dictionary<string, string> skills, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in skills)
                    {
                        if (dbCtx.Skills.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.Skills.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == q.Value) continue;
                            existingItem.name = q.Value;
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.Skills.Add(new Skill() { id = q.Key, name = q.Value });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Skills added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SaveUsers(Dictionary<string, string> users, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in users)
                    {
                        if (dbCtx.Users.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.Users.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem.name == q.Value.Split('~')[0] && existingItem.employeeId == q.Value.Split('~')[1] && existingItem.emailAddress == q.Value.Split('~')[3] && existingItem.state == q.Value.Split('~')[4]) continue;
                            existingItem.name = q.Value.Split('~')[0];
                            existingItem.employeeId = q.Value.Split('~')[1];
                            existingItem.emailAddress = q.Value.Split('~')[3];
                            existingItem.state = q.Value.Split('~')[4];
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.Users.Add(new User()
                            {
                                id = q.Key,
                                name = q.Value.Split('~')[0],
                                employeeId = q.Value.Split('~')[1],
                                emailAddress = q.Value.Split('~')[3],
                                state = q.Value.Split('~')[4]
                            });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Users added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
            SaveUserSkills(users, connectionString);
        }
        private static void SaveUserSkills(Dictionary<string, string> users, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var user in users)
                    {
                        var userDetails = user.Value.Split('~');
                        var userSkillsFromGC = new List<UserSkill>();
                        if (userDetails.Count() > 1 && !string.IsNullOrEmpty(userDetails[2]))
                        {
                            var userSkills = userDetails[2].Split('|');
                            foreach (var skill in userSkills)
                            {
                                string skillId = string.Empty;
                                int proficiencyValue = 0;
                                if (skill.Split('&').Count() > 0 && skill.Split('&')[0].Length > 0)
                                {
                                    skillId = skill.Split('&')[0];
                                }
                                if (skill.Split('&').Count() > 1 && skill.Split('&')[1].Length > 0)
                                {
                                    proficiencyValue = Convert.ToInt32(skill.Split('&')[1]);
                                }
                                if (skillId.Length > 0 || proficiencyValue > 0)
                                {
                                    userSkillsFromGC.Add(new UserSkill()
                                    {
                                        userId = user.Key,
                                        skillId = skillId,
                                        proficiency = proficiencyValue
                                    });
                                }
                            }
                        }

                        if (dbCtx.UserSkills.Any(x => x.userId == user.Key)) // does id exist
                        {
                            if (user.Value == null) continue;
                            var existingDBUserSkills = dbCtx.UserSkills.Where(x => x.userId == user.Key);
                            foreach (UserSkill item in existingDBUserSkills)
                            {
                                if (userSkillsFromGC.Count(x => x.skillId == item.skillId) <= 0)
                                {
                                    dbCtx.UserSkills.Remove(item);
                                    counter.RowsRemovedFromTheBag++;
                                }
                            }
                            foreach (var userSkill in userSkillsFromGC)
                            {
                                if (dbCtx.UserSkills.Any(x => x.userId == userSkill.userId && x.skillId == userSkill.skillId)) // does id exist
                                {
                                    // update skill if id does exist
                                    if (user.Value == null) continue;
                                    var existingItem = dbCtx.UserSkills.FirstOrDefault(x => x.userId == userSkill.userId && x.skillId == userSkill.skillId);
                                    if (existingItem.skillId == userSkill.skillId && existingItem.proficiency == userSkill.proficiency) continue;
                                    existingItem.skillId = userSkill.skillId;
                                    existingItem.proficiency = userSkill.proficiency;
                                    counter.RowsUpdated++;
                                }
                                else
                                {
                                    // create an skill if id doesn't exist
                                    dbCtx.UserSkills.Add(new UserSkill()
                                    {
                                        userId = userSkill.userId,
                                        skillId = userSkill.skillId,
                                        proficiency = userSkill.proficiency
                                    });
                                    counter.RowsAdded++;
                                }
                            }
                        }
                        else
                        {
                            foreach (var skill in userSkillsFromGC)
                            {
                                dbCtx.UserSkills.Add(new UserSkill()
                                {
                                    userId = skill.userId,
                                    skillId = skill.skillId,
                                    proficiency = skill.proficiency
                                });
                                counter.RowsAdded++;
                            }
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Users added:{counter.RowsAdded}, updated:{counter.RowsUpdated}, deleted: {counter.RowsRemovedFromTheBag}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SaveWrapUpCodes(Dictionary<string, string> wrapUpCodes, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in wrapUpCodes)
                    {
                        if (dbCtx.WrapUpCodes.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.WrapUpCodes.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == q.Value) continue;
                            existingItem.name = q.Value;
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.WrapUpCodes.Add(new WrapUpCode() { id = q.Key, name = q.Value });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Wrap Up Codes added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SaveEdgeServers(Dictionary<string, string> edgeServers, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in edgeServers)
                    {
                        if (dbCtx.EdgeServers.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.EdgeServers.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == q.Value) continue;
                            existingItem.name = q.Value;
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.EdgeServers.Add(new EdgeServer() { id = q.Key, name = q.Value });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Edge Servers added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SaveCampaigns(Dictionary<string, string> campaigns, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in campaigns)
                    {
                        if (dbCtx.Campaigns.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.Campaigns.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == q.Value) continue;
                            existingItem.name = q.Value;
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.Campaigns.Add(new Campaign() { id = q.Key, name = q.Value });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Campaigns added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SaveContactLists(Dictionary<string, string> contactLists, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in contactLists)
                    {
                        if (dbCtx.ContactLists.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (q.Value == null) continue;
                            var existingItem = dbCtx.ContactLists.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == q.Value) continue;
                            existingItem.name = q.Value;
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.ContactLists.Add(new ContactList() { id = q.Key, name = q.Value });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"Contact lists added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }

        public static void SavePresenceDefinitions(Dictionary<string, string> presence, string connectionString)
        {
            using (var dbCtx = new DatabaseContext(connectionString))
            {
                try
                {
                    var counter = new TransactionCounter();
                    foreach (var q in presence)
                    {
                        var pDef = q.Value.Split('|');

                        if (dbCtx.PresenceDefinitions.Any(x => x.id == q.Key)) // does id exist
                        {
                            // update name if id does exist
                            if (pDef[0] == null) continue;
                            var existingItem = dbCtx.PresenceDefinitions.FirstOrDefault(x => x.id == q.Key);
                            if (existingItem == null || existingItem.name == pDef[0] || existingItem.systemPresence == pDef[1]) continue;
                            existingItem.name = pDef[0];
                            existingItem.systemPresence = pDef[1];
                            counter.RowsUpdated++;
                        }
                        else
                        {
                            // create an item if id doesn't exist
                            dbCtx.PresenceDefinitions.Add(new PresenceDefinitions() { id = q.Key, name = pDef[0], systemPresence = pDef[1] });
                            counter.RowsAdded++;
                        }
                    }
                    dbCtx.SaveChanges();
                    Trace.Info($"PresenceDefinitions added:{counter.RowsAdded}, updated:{counter.RowsUpdated}");
                }
                catch (Exception ex)
                {
                    Trace.Fatal(ex);
                }
            }
        }


    }
}
