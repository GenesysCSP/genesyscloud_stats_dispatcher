﻿using System.ComponentModel.DataAnnotations;

namespace gcsd.plugin.sql.DataModel.Dictionary
{
    public class User : IDictionaryItem, IUserItem
    {
        [Key]
        public string id { get; set; }
        public string name { get; set; }
        public string employeeId { get ; set ; }
        public string emailAddress { get; set; }
        public string state { get; set; }
    }

}
