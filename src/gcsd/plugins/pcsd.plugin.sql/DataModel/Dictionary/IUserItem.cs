﻿namespace gcsd.plugin.sql.DataModel.Dictionary
{
    public interface IUserItem
    {
        string id { get; set; }
        string name { get; set; }
        string employeeId { get; set; }
        string emailAddress { get; set; }
    }
}
