﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gcsd.plugin.sql.DataModel.Participant
{
    public class ParticipantAttr
    {
        [Key]
        [Column(Order = 1)]
        public string conversationId { get; set; }
        [Key]
        [Column(Order = 2)]
        public string participantId { get; set; }
        public string attrName { get; set; }
        public string attrValue { get; set; }
    }
}
