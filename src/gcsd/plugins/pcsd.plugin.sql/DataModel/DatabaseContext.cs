﻿using System.Data.Entity;
using gcsd.plugin.sql.DataModel.Conversation;
using gcsd.plugin.sql.DataModel.ConversationAggregates;
using gcsd.plugin.sql.DataModel.Dictionary;
using gcsd.plugin.sql.DataModel.UserAggregates;
using gcsd.plugin.sql.DataModel.UserDetails;

namespace gcsd.plugin.sql.DataModel
{
    public class DatabaseContext: DbContext
    {
        public DatabaseContext()
        {
            // empty constructor for db migrations
        }

        public DatabaseContext(string connectionString) { Database.Connection.ConnectionString = connectionString;}

        public DbSet<Interval.Interval> Intervals { get; set; }

        public DbSet<Conversation.Conversation> Conversations { get; set; }

        public DbSet<OngoingConversation> OngoingConversation { get; set; }

        public DbSet<PrimaryPresence> PrimaryPresence { get; set; }

        public DbSet<RoutingStatus> RoutingStatus { get; set; }

        public DbSet<Queue> Queues { get; set; }

        public DbSet<Language> Languages { get; set; }

        public DbSet<Skill> Skills { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<WrapUpCode> WrapUpCodes { get; set; }

        public DbSet<EdgeServer> EdgeServers { get; set; }

        public DbSet<Campaign> Campaigns { get; set; }

        public DbSet<ContactList> ContactLists { get; set; }

        public DbSet<PresenceDefinitions> PresenceDefinitions { get; set; }

        public DbSet<ParticipantAttr.ParticipantAttr> ParticipantAttrs { get; set; }

        public DbSet<ConversationAggregate> ConversationAggregates { get; set; }

        public DbSet<UserAggregate> UserAggregates { get; set; }

        public DbSet<UserSkill> UserSkills { get; set; }
    }
}
