﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gcsd.plugin.sql.DataModel.Conversation
{
    public class RoutingSkill
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long RowId { get; set; }        
        public string RequestedRoutingSkillId { get; set; }
    }
}
