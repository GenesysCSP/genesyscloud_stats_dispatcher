﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace gcsd.plugin.sql.DataModel.Conversation
{
    public class Conversation
    {
        [Key]
        public string conversationId { get; set; }
        public DateTime? conversationStart { get; set; }
        public DateTime? conversationEnd { get; set; }
        public IList<Participant> participants { get; set; }        
        public bool IsFinished() => conversationEnd != null;
    }
}
