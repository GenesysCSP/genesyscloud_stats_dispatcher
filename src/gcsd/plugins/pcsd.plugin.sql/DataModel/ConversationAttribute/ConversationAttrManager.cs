﻿using System;
using System.Collections.Generic;
using log4net;
using System.Reflection;
using System.Web.Script.Serialization;
using gcsd.plugin.sql.DataModel.Participant;

namespace gcsd.plugin.sql.DataModel.ConversationAttribute
{
    public class ConversationAttrManager
    {
        public IList<ConversationAttr> conversationsAttr { get; set; }
        private static readonly ILog Trace = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static IList<ParticipantAttr> ParseConversationAttr(string jsonText)
        {
            var result = new List<ParticipantAttr>();
            try
            {
                var json = new JavaScriptSerializer { MaxJsonLength = int.MaxValue };
                var parsed = json.Deserialize<ConversationAttrManager>(jsonText);
                var conversations = parsed.conversationsAttr;
                foreach (var conversation in conversations)
                {
                    foreach (var participant in conversation.participants)
                    {
                        result.Add(new ParticipantAttr()
                        {
                            conversationId = conversation.id,
                            participantId = participant.id,
                            attrName = participant.attributes.cnx_Environment
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.Fatal(ex);
            }
            return result;
        }
    }
}
