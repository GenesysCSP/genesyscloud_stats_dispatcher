﻿using System.Collections.Generic;

namespace gcsd.plugin.sql.DataModel.ConversationAttribute
{
    public class ConversationAttr
    {
        public string id { get; set; }
        public IList<Participant> participants { get; set; }
    }

    public class Participant
    {
        public string id { get; set; }
        public Attributes attributes { get; set; }
    }

    public class Attributes
    {
        public string cnx_Full_Name { get; set; }
        public string cnx_Language { get; set; }
        public string cnx_Environment { get; set; }
        public string membershipID { get; set; }
    }
}
