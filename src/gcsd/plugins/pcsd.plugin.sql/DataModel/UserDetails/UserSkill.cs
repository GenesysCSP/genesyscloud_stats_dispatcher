﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gcsd.plugin.sql.DataModel.UserDetails
{
    public class UserSkill
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long id { get; set; }
        public string userId { get; set; } 
        public string skillId { get; set; }
        public int proficiency { get; set; }
    }
}
