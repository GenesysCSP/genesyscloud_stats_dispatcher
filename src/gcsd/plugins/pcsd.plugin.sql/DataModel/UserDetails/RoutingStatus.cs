﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace gcsd.plugin.sql.DataModel.UserDetails
{
    public class RoutingStatus
    {        
        [Key]
        [Column(Order = 1)]
        [StringLength(128)]
        public string userId { get; set; }
        [Key]
        [Column(Order = 2, TypeName = "datetime2")]        
        public DateTime startTime { get; set; }
        [Column(Order = 3, TypeName = "datetime2")]
        public DateTime endTime { get; set; }
        [Key]
        [Column(Order = 4)]
        [StringLength(128)]
        public string routingStatus { get; set; }
    }

    public class RoutingStatusComparer : System.Collections.Generic.IEqualityComparer<RoutingStatus>
    {
        public bool Equals(RoutingStatus x, RoutingStatus y)
        {
            if (String.Equals(x.userId, y.userId, StringComparison.OrdinalIgnoreCase) && String.Equals(x.routingStatus, y.routingStatus, StringComparison.OrdinalIgnoreCase) && String.Equals(x.startTime.ToString(), y.startTime.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                return (true);
            }
            else
            {
                return (false);
            }
        }

        public int GetHashCode(RoutingStatus obj)
        {
            return base.GetHashCode();
        }

    }
}
