# PureCloud Stats Dispatcher 

```
This is an open source software without any warranty or free support. 
```


**Do NOT post any messages on the PureCloud developer's forum about this application as this is not an official Genesys application.**
**If you are looking for an officially supported app, check the [Open Data Exporter](https://github.com/MyPureCloud/open-data-exporter) app on Github.**


This Windows console application uses the PureCloud API to export analytics to a 3rd party `target` (ie: SQL server, Console) and then exits. It pulls all analytics data available from PureCloud for the last 7 days and pushes it to the targets specified on the command line.

**Please note that, at this time, only exports to Microsoft SQL Server and CSV files are supported.**

Statistics include:

  * [Conversations](https://developer.mypurecloud.com/api/rest/v2/analytics/conversation.html). To get a description of each field, look under the `200` response [here](https://developer.mypurecloud.com/api/rest/v2/analytics/index.html#postAnalyticsConversationsDetailsQuery)
  * Queue: See the `200` response [here](https://developer.mypurecloud.com/api/rest/v2/analytics/#postAnalyticsConversationsAggregatesQuery) for details
  * Users: See the `200` response [here](https://developer.mypurecloud.com/api/rest/v2/analytics/#postAnalyticsUsersAggregatesQuery) for details

![Architecture.jpg](https://bitbucket.org/repo/8r9d86/images/2752406409-Architecture.jpg)

### Installation ###
Download the latest release: [here](https://bitbucket.org/eccemea/purecloud-stats-dispatcher/downloads/)

Launch the MSI installer and follow installation steps.

### Usage: ###
```
PCSD.exe /clientid= /clientsecret= /environment=mypurecloud.ie /target-<target name>=<options>
```

It is recommended to use a scheduling software (i.e. Windows' scheduled task) to run this application regularly.

#### Parameters ####

* /clientid: PureCloud OAuth Client Id. Follow these [instructions](https://bitbucket.org/GenesysCSP/purecloud-stats-dispatcher/wiki/Home) to get yours.
* /clientsecret: PureCloud OAuth Client Secret. Follow these [instructions](https://bitbucket.org/GenesysCSP/purecloud-stats-dispatcher/wiki/Home) to get yours.
* /environment: PureCloud Environment. Depends on where your org is based. Possible values are:
    * `mypurecloud.ie` (EMEA)
    * `mypurecloud.com` (USA)
    * `mypurecloud.com.au` (Australia/New Zealand)
    * `mypurecloud.jp` (Japan)
    * `ininsca.com` (Only for ININ internals)
* /target-*: Targets to push the data to. Possible values as of today are:
    * csv: (`/target-csv`) to push data to CSV file
    * sql: (`/target-sql`) to push data to your Microsoft SQL Server 2012 instance
                * this is the SQL Connection String The connection string depend on the specific data source for this connection. It should have rights to create a new database and tables, this is because the first time the application will create a new database and tables if these do not exists. However, the application does not need delete or edit existing databases and tables. 
    * Multiple targets can be used at the same time

#### Optional Parameter(s) ####
* /stats=<stats to retrieve>: Selects which stats need to be retrieved from PureCloud. Possible values for /stats are:
    * conversations: conversation details data
    * queues: queue aggregates data
    * users: user aggregates data
    * userdetails: user details data
    * For example, to retrieve conversation-related data, add `/stats=conversations` to the list of command-line parameters. You can combine more than one filter, i.e. to retrieve conversation and queue-related data, add `/stats=conversations,queues` to the list of command-line parameters.
    * By default (if /stats is not specified), PCSD will retrieve ALL (conversations,queues,users) data.
* /startdate=<YYYY-MM-DD hh:mm:ss>: Use this parameter to specify the start date from when PCSD will pull data from PureCloud, i.e. `/startdate=2015-12-31` pulls data starting from December, 31st 2015 at Midnight. `/startdate="2015-12-31 14:00:00"` will pull data from 2 PM the same day. The local Amazon data center time should be used (i.e. for mypurecloud.ie (EMEA), the data center is based in Ireland, GMT).
    * If this parameter is not specified, PCSD will pull data from the last 7 days.

### Using the SQL plugin ###
To push data to a SQL server, you can use the SQL plugin (shipped with this application by default) like this: `PCSD.exe /clientid=your_client_id /clientsecret=your_client_secret /environment=mypurecloud.com /target-sql="your_connection_string>"`

Sample connection string is `"Server=mysqlserver;Database=mydatabase;User Id=myadmin;Password=mypassword"`.

* Server: your SQL server
* Database: your database name. It is recommended to create a new database just for that purpose.
* User Id: SQL user with admin privileges on your database
* Password: well... the user's password :)

The complete command is: `PCSD.exe /clientid=your_client_id /clientsecret=your_client_secret /environment=mypurecloud.com /target-sql="Server=mysqlserver;Database=mydatabase;User Id=myadmin;Password=mypassword"`

The following SQL tables will be created during the first execution:

* Campaigns: Dictionary of campaigns.
* ContactLists: Dictionary of contact lists.
* ConversationAggregates: Aggregated interval data for queues.
* Conversations: Logical units that captures an entire interaction.
* Participants: Human or system actors involved in a conversation, such as an outside caller, ACD (automatic call distributor), IVR (interactive voice response), or contact center agent.
* Sessions: The media channels used for interactions, such as phone, web chat, or email.
* Segments: Media-specific conversation states, such as phone ringing, phone connected, or call held. Segments are the building blocks of a call, and are ideal for viewing how an individual call was handled. For more information, see [conversation detail query](https://developer.mypurecloud.com/api/rest/v2/analytics/conversation.html).
* Properties: ?
* RoutingSkills: Skill Ids used during conversations.
* EdgeServers: Dictionary of edge servers.
* Intervals: Remembers last intervals used during previous exporting process. Do not change any data in this table.
* Languages: Language requested by the conversations.
* OngoingConversations: List of interactions that were ongoing during exporting process. Do not change any data in this table.
* ParticipantAttrs: participant attributes
* PresenceDefinitions: Dictionary of presence definitions.
* Queues: Dictionary of queues.
* Skills: Dictionaty of skills.
* UserAggregates: Aggregated interval data for users.
* Users: Dictionary of users.
* WrapUpCodes: Dictionary of wrapup codes.
* PrimaryPresences: User details - primary presence history
* RoutingStatus: User details - routing status history

Sample SQL views have been added for your convenience:

* [vw_Conversations](https://bitbucket.org/eccemea/purecloud-stats-dispatcher/src/c2d73c2fc952eb8d80ad44c6ed52f9ac45ac1cc5/src/gcsd/plugins/gcsd.plugin.sql/Views/vw_Conversations.sql)
* [vw_Queues](https://bitbucket.org/eccemea/purecloud-stats-dispatcher/src/19ab7699f3cc41447da172856f81f24c22fce69c/src/gcsd/plugins/gcsd.plugin.sql/Views/vw_Queues.sql)
* [vw_Users](https://bitbucket.org/eccemea/purecloud-stats-dispatcher/src/19ab7699f3cc41447da172856f81f24c22fce69c/src/gcsd/plugins/gcsd.plugin.sql/Views/vw_Users.sql)

When executing the scripts above, make sure you select the correct database.

###Using the CSV plugin###

In order to push the data to CSV file you have to use the CSV plugin (it is installed with the application by default). The following command shows how to use the plugin:

`PCSD.exe /clientid={your_client_id} /clientsecret={your_client_secret} /environment=mypurecloud.ie /target-csv=�{output_file_path}�`

The data will be retrieved from the PureCloud API and pushed to the file specified in /target-csv parameter.

####For advanced users only####
The output format can be modified by changing a mapping file. It is located into installation folder: Plugins\gcsd.plugin.csv.mapping.json. The file format is JSON. It is an array of objects. Each object represents a separate column in the output file and it must contains a following property:

* path (string) - Path points out on the property in the API response.

Objects also can optionally contain below properties:

* dictionary (string) - If present it means that the CSV plugin has to use the dictionary to replace GUID value for a friendly name. Following dictionaries are supported: users, queues, skills, languages, wrapupcodes, edgeservers, campaigns, contactlists.
* isArray (bool) - If true it means that a source value is an array of strings that have to be joined into one string.
* clean (bool) - If true it means that the plugin will clean up a value for the field. E.g. end-line characters are replaced by space. It is usable option for free-text values that could break the CSV file structure.
* maxLen (int) - If present it means that a string value longer than a given number will be shortened.
* dateTimeMask - This property contains a mask of date/time values. It supports .NET date/time format strings. Learn more about format strings: [Standard format strings](https://msdn.microsoft.com/en-us/library/az4se3k1(v=vs.110).aspx),  [Custom format strings](https://msdn.microsoft.com/en-us/library/8kb3ddd4(v=vs.110).aspx).

Sample representation of requestedRoutingSkillIds field:
```   
{
   "path": "conversations.participants.sessions.segments.requestedRoutingSkillIds",
   "dictionary": "skills",
   "isArray": true
}
```

### Extensibility ###
This application supports the use of plugins to create custom targets. See [this](https://bitbucket.org/GenesysCSP/purecloud-stats-dispatcher/wiki/Creating%20plugins) page for details.

### Trace file logging ###
This application supports the use of trace logging via a log file.

How to configure trace logging

* Backup gcsd.exe.config
* Edit gcsd.exe.config with a text editor
* Add a file appender section add the text below after the </logger> section:

```
#!xml

    <appender name="RollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="**### Trace.log ###**" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <datePattern value="yyyy" />
      <maxSizeRollBackups value="20" />
      <maximumFileSize value="20MB" />
      <StaticLogFileName value="false" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%d{dd MMM yyyy HH:mm:ss,fff} [%thread] %-20.20M - %m%n%exception" />
      </layout>
    </appender>
```

* Logger section add **<appender-ref ref="RollingLogFileAppender" />** in the logger section:
 
```
#!xml

    <logger name="gcsd" additivity="false">
      <level value="ALL" />
      <appender-ref ref="RollingLogFileAppender" />
      <appender-ref ref="Console" />
    </logger>
```

* Increase the log level to all:

```
#!xml

<level value="ALL" />
```
 instead of the default 
```
#!xml

<level value="Info" />
```

### Self Update PureCloud Libraries ###

To work with code you should have installed:

* Visual Studio 201X

* .Net Core


If you want to manually, on your own update PureCloud Libraries to the newest one first download entire repository.

1. open Solution

2. navigate to Solution Explorer

3. right click on gcsd project and select option "Manage NuGet Packages..."

4. switch tab to "Updates"

5. find and select checkbox next to package named "PureCloudPlatform.Client.V2"

6. click on Update button

7. make sure that all test will pass after upgrade (Tests -> Run -> All tests)



After a while latest version of PureCloud libraries will be downloaded and updated.
The last thing is to increase version and rebuild entire solution.

1. To increase version, navigate to your project and click properties. Then inside Application section click button "Assembly Information". In new window update version number and click Apply button.

2. To recompile solution right click on main solution item and choose "Rebuild Solution"
